//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bFindClick(TObject *Sender)
{
	ttm->DiscoverManagers();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
	ttm->DiscoverManagers();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttmEndProfilesDiscovery(TObject * const Sender, TTetheringProfileInfoList * const ARemoteProfiles)

{
	lb->Clear();

	for (int i = 0; i < ttm->RemoteProfiles->Count; i++)
	{
		lb->Items->Add(ttm->RemoteProfiles->Items[i].ProfileText);
	}

	if(lb->Count > 0)
	{
		lb->ItemIndex = 0;
		ttp->Connect(ttm->RemoteProfiles->Items[lb->ItemIndex]);
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttmEndManagersDiscovery(TObject * const Sender, TTetheringManagerInfoList * const ARemoteManagers)

{
	for (int i = 0; i < ARemoteManagers->Count; i++)
	{
		ttm->PairManager(
			const_cast<TTetheringManagerInfoList*>(ARemoteManagers)->Items[i]);
	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttpResources0ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
	tbVolume->Value = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttpResources1ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
	tbVolume->Max = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttpResources2ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
    tbTime->Value = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttpResources3ResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
	tbTime->Max = AResource->Value.AsSingle;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bSendStringClick(TObject *Sender)
{
	ttp->SendString(ttm->RemoteProfiles->Items[lb->ItemIndex], "ABCD", edString->Text);
}
//---------------------------------------------------------------------------
