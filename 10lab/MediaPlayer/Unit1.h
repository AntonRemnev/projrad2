//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TActionList *al;
	TLayout *Layout1;
	TToolBar *ToolBar1;
	TButton *bFind;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TListBox *lb;
	TLayout *Layout2;
	TButton *bPlay;
	TButton *bStop;
	TButton *bClear;
	TTrackBar *tbVolume;
	TTrackBar *tbTime;
	TEdit *edString;
	TButton *bSendString;
	TAction *acPlayPause;
	TAction *acStop;
	TAction *acClear;
	void __fastcall bFindClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall ttmEndProfilesDiscovery(TObject * const Sender, TTetheringProfileInfoList * const ARemoteProfiles);
	void __fastcall ttmEndManagersDiscovery(TObject * const Sender, TTetheringManagerInfoList * const ARemoteManagers);
	void __fastcall ttpResources0ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources1ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources2ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources3ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall bSendStringClick(TObject *Sender);







private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
