//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Dialogs.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Media.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdActns.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TActionList *al;
	TMediaPlayerStop *acMediaPlayerStop;
	TMediaPlayerPlayPause *acMediaPlayerPlayPause;
	TMediaPlayerCurrentTime *acMediaPlayerCurrentTime;
	TMediaPlayerVolume *acMediaPlayerVolume;
	TAction *acClear;
	TAction *acPlayPause;
	TAction *acStop;
	TLayout *lyTime;
	TLayout *lyVolume;
	TMemo *meLog;
	TToolBar *ToolBar1;
	TButton *bOpen;
	TButton *bPlay;
	TButton *bStop;
	TButton *bClear;
	TLabel *laVolumeCaption;
	TLabel *laVolume;
	TTrackBar *tbVolume;
	TMediaPlayer *mp;
	TMediaPlayerControl *mpc;
	TOpenDialog *od;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	TLabel *laTime;
	TTrackBar *tbTime;
	TLabel *laDuration;
	void __fastcall bOpenClick(TObject *Sender);
	void __fastcall acPlayPauseExecute(TObject *Sender);
	void __fastcall acStopExecute(TObject *Sender);
	void __fastcall tbVolumeChange(TObject *Sender);
	void __fastcall tbTimeChange(TObject *Sender);
	void __fastcall ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
