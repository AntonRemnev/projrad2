//---------------------------------------------------------------------------

#pragma hdrstop

#include "dmuDatabase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TdmDatabase::TdmDatabase(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TdmDatabase::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TdmDatabase::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------

void __fastcall TdmDatabase::ToyshopConnectionAfterConnect(TObject *Sender)
{
	OrdersTable->Open();
    ProductsTable->Open();
}
//---------------------------------------------------------------------------
void __fastcall TdmDatabase::ToyshopConnectionBeforeConnect(TObject *Sender)
{
     ToyshopConnection->Params->Values["DataBase"] = "..\\..\\..\\TOYSHOP.FDB";
}
//---------------------------------------------------------------------------


void TdmDatabase::InsertOrder(int aPRODUCT_ID, UnicodeString aPHONE, UnicodeString aEMAIL, UnicodeString aADDRESS, int aPRICE){
	Orders_insProc->Params->ParamByName("PRODUCT_ID")->Value = aPRODUCT_ID;
	Orders_insProc->Params->ParamByName("PHONE")->Value = aPHONE;
	Orders_insProc->Params->ParamByName("EMAIL")->Value = aEMAIL;
	Orders_insProc->Params->ParamByName("ADDRESS")->Value = aADDRESS;
	Orders_insProc->Params->ParamByName("PRICE")->Value = aPRICE;
	Orders_insProc->Params->ParamByName("STATUS")->Value = "���������";
    Orders_insProc->ExecProc();

}
