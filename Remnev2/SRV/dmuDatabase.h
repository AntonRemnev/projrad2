//---------------------------------------------------------------------------

#ifndef dmuDatabaseH
#define dmuDatabaseH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TdmDatabase : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *ToyshopConnection;
	TSQLDataSet *OrdersTable;
	TSQLDataSet *ProductsTable;
	TSQLStoredProc *Orders_insProc;
	TDataSetProvider *dspOrders;
	TDataSetProvider *dspProducts;
	TIntegerField *OrdersTableID;
	TIntegerField *OrdersTablePRODUCT_ID;
	TWideStringField *OrdersTablePHONE;
	TWideStringField *OrdersTableEMAIL;
	TWideStringField *OrdersTableADDRESS;
	TIntegerField *OrdersTablePRICE;
	TWideStringField *OrdersTableSTATUS;
	TIntegerField *ProductsTableID;
	TWideStringField *ProductsTableTITLE;
	TIntegerField *ProductsTablePRICE;
	TWideStringField *ProductsTableDESCTIPTION;
	TBlobField *ProductsTableIMAGE;
	void __fastcall ToyshopConnectionAfterConnect(TObject *Sender);
	void __fastcall ToyshopConnectionBeforeConnect(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TdmDatabase(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
	void InsertOrder(int aPRODUCT_ID, UnicodeString aPHONE, UnicodeString aEMAIL, UnicodeString aADDRESS, int aPRICE);
};
//---------------------------------------------------------------------------
extern PACKAGE TdmDatabase *dmDatabase;
//---------------------------------------------------------------------------
#endif

