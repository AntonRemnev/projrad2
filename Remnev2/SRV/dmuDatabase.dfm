object dmDatabase: TdmDatabase
  OldCreateOrder = False
  Height = 353
  Width = 500
  object ToyshopConnection: TSQLConnection
    ConnectionName = 'ToyShop'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=Remnev2\TOYSHOP.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    AfterConnect = ToyshopConnectionAfterConnect
    BeforeConnect = ToyshopConnectionBeforeConnect
    Connected = True
    Left = 55
    Top = 33
  end
  object OrdersTable: TSQLDataSet
    Active = True
    CommandText = 'ORDERS'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = ToyshopConnection
    Left = 53
    Top = 132
    object OrdersTableID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object OrdersTablePRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object OrdersTablePHONE: TWideStringField
      FieldName = 'PHONE'
      Required = True
      Size = 1200
    end
    object OrdersTableEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Required = True
      Size = 1200
    end
    object OrdersTableADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Required = True
      Size = 2000
    end
    object OrdersTablePRICE: TIntegerField
      FieldName = 'PRICE'
      Required = True
    end
    object OrdersTableSTATUS: TWideStringField
      FieldName = 'STATUS'
      Required = True
      Size = 1200
    end
  end
  object ProductsTable: TSQLDataSet
    Active = True
    CommandText = 'PRODUCTS'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = ToyshopConnection
    Left = 158
    Top = 133
    object ProductsTableID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object ProductsTableTITLE: TWideStringField
      FieldName = 'TITLE'
      Required = True
      Size = 1200
    end
    object ProductsTablePRICE: TIntegerField
      FieldName = 'PRICE'
      Required = True
    end
    object ProductsTableDESCTIPTION: TWideStringField
      FieldName = 'DESCTIPTION'
      Required = True
      Size = 12000
    end
    object ProductsTableIMAGE: TBlobField
      FieldName = 'IMAGE'
      Size = 1
    end
  end
  object Orders_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'PRODUCT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 300
        Name = 'PHONE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 300
        Name = 'EMAIL'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 500
        Name = 'ADDRESS'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 4
        Name = 'PRICE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 300
        Name = 'STATUS'
        ParamType = ptInput
      end>
    SQLConnection = ToyshopConnection
    StoredProcName = 'ORDERS_INS'
    Left = 310
    Top = 138
  end
  object dspOrders: TDataSetProvider
    DataSet = OrdersTable
    Left = 56
    Top = 240
  end
  object dspProducts: TDataSetProvider
    DataSet = ProductsTable
    Left = 160
    Top = 240
  end
end
