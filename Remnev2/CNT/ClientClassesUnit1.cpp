// 
// Created by the DataSnap proxy generator.
// 19.01.2018 11:56:09
// 

#include "ClientClassesUnit1.h"

void __fastcall TdmDatabaseClient::ToyshopConnectionAfterConnect(TJSONObject* Sender)
{
  if (FToyshopConnectionAfterConnectCommand == NULL)
  {
    FToyshopConnectionAfterConnectCommand = FDBXConnection->CreateCommand();
    FToyshopConnectionAfterConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FToyshopConnectionAfterConnectCommand->Text = "TdmDatabase.ToyshopConnectionAfterConnect";
    FToyshopConnectionAfterConnectCommand->Prepare();
  }
  FToyshopConnectionAfterConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FToyshopConnectionAfterConnectCommand->ExecuteUpdate();
}

void __fastcall TdmDatabaseClient::ToyshopConnectionBeforeConnect(TJSONObject* Sender)
{
  if (FToyshopConnectionBeforeConnectCommand == NULL)
  {
    FToyshopConnectionBeforeConnectCommand = FDBXConnection->CreateCommand();
    FToyshopConnectionBeforeConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FToyshopConnectionBeforeConnectCommand->Text = "TdmDatabase.ToyshopConnectionBeforeConnect";
    FToyshopConnectionBeforeConnectCommand->Prepare();
  }
  FToyshopConnectionBeforeConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FToyshopConnectionBeforeConnectCommand->ExecuteUpdate();
}

System::UnicodeString __fastcall TdmDatabaseClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmDatabase.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdmDatabaseClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmDatabase.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TdmDatabaseClient::InsertOrder(int aPRODUCT_ID, System::UnicodeString aPHONE, System::UnicodeString aEMAIL, System::UnicodeString aADDRESS, int aPRICE)
{
  if (FInsertOrderCommand == NULL)
  {
    FInsertOrderCommand = FDBXConnection->CreateCommand();
    FInsertOrderCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FInsertOrderCommand->Text = "TdmDatabase.InsertOrder";
    FInsertOrderCommand->Prepare();
  }
  FInsertOrderCommand->Parameters->Parameter[0]->Value->SetInt32(aPRODUCT_ID);
  FInsertOrderCommand->Parameters->Parameter[1]->Value->SetWideString(aPHONE);
  FInsertOrderCommand->Parameters->Parameter[2]->Value->SetWideString(aEMAIL);
  FInsertOrderCommand->Parameters->Parameter[3]->Value->SetWideString(aADDRESS);
  FInsertOrderCommand->Parameters->Parameter[4]->Value->SetInt32(aPRICE);
  FInsertOrderCommand->ExecuteUpdate();
}


__fastcall  TdmDatabaseClient::TdmDatabaseClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TdmDatabaseClient::TdmDatabaseClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmDatabaseClient::~TdmDatabaseClient()
{
  delete FToyshopConnectionAfterConnectCommand;
  delete FToyshopConnectionBeforeConnectCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FInsertOrderCommand;
}

