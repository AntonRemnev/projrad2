//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiProducts;
	TTabItem *tiProduct;
	TTabItem *tiNewOrder;
	TLayout *Layout1;
	TButton *buProducts;
	TButton *buExit;
	TButton *buOrders;
	TLabel *Label1;
	TTabItem *tiOrders;
	TScrollBox *sbItems;
	TImage *imProduct;
	TLabel *laProductTitle;
	TLabel *laProductPrice;
	TMemo *meProductDescription;
	TButton *buNewOrder;
	TLayout *Layout2;
	TLayout *Layout3;
	TLabel *Label2;
	TEdit *edOrderToyTitle;
	TLayout *Layout4;
	TLabel *Label3;
	TEdit *edOrderPrice;
	TLayout *Layout5;
	TLabel *Label4;
	TEdit *edOrderAddress;
	TLayout *Layout6;
	TLabel *Label5;
	TEdit *edOrderEmail;
	TLayout *Layout7;
	TLabel *Label6;
	TEdit *edOrderPhone;
	TButton *buMakeOrder;
	TListView *lvOrders;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TToolBar *ToolBar1;
	TButton *buBack;
	TLabel *Label7;
	TToolBar *ToolBar2;
	TButton *buBackProducts;
	TToolBar *ToolBar3;
	TButton *buBackProduct;
	TLabel *Label8;
	TToolBar *ToolBar4;
	TButton *buMenu;
	TLabel *Label9;
	void __fastcall buProductsClick(TObject *Sender);
	void __fastcall buOrdersClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall buNewOrderClick(TObject *Sender);
	void __fastcall buMakeOrderClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buBackProductClick(TObject *Sender);
	void __fastcall buBackProductsClick(TObject *Sender);
private:	// User declarations
	void ShowProducts();
	void MakeOrder(int aPRODUCT_ID, UnicodeString aPHONE, UnicodeString aEMAIL, UnicodeString aADDRESS, int aPRICE);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	void __fastcall ShowProduct(TObject *Sender);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
