//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "ClientClassesUnit1.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
#include <IPPeerClient.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *ToyShopConnection;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *cdsProducts;
	TClientDataSet *cdsOrders;
	TIntegerField *cdsProductsID;
	TWideStringField *cdsProductsTITLE;
	TIntegerField *cdsProductsPRICE;
	TWideStringField *cdsProductsDESCTIPTION;
	TBlobField *cdsProductsIMAGE;
	TIntegerField *cdsOrdersID;
	TIntegerField *cdsOrdersPRODUCT_ID;
	TWideStringField *cdsOrdersPHONE;
	TWideStringField *cdsOrdersEMAIL;
	TWideStringField *cdsOrdersADDRESS;
	TIntegerField *cdsOrdersPRICE;
	TWideStringField *cdsOrdersSTATUS;
	void __fastcall ToyShopConnectionAfterConnect(TObject *Sender);
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TdmDatabaseClient* FdmDatabaseClient;
	TdmDatabaseClient* GetdmDatabaseClient(void);
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	__fastcall ~Tdm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmDatabaseClient* dmDatabaseClient = {read=GetdmDatabaseClient, write=FdmDatabaseClient};
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
