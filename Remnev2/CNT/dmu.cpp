//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall Tdm::~Tdm()
{
	delete FdmDatabaseClient;
}

TdmDatabaseClient* Tdm::GetdmDatabaseClient(void)
{
	if (FdmDatabaseClient == NULL)
	{
		ToyShopConnection->Open();
		FdmDatabaseClient = new TdmDatabaseClient(ToyShopConnection->DBXConnection, FInstanceOwner);
	}
	return FdmDatabaseClient;
};

void __fastcall Tdm::ToyShopConnectionAfterConnect(TObject *Sender)
{
	cdsOrders->Open();
	cdsProducts->Open();
}
//---------------------------------------------------------------------------
void __fastcall Tdm::DataModuleCreate(TObject *Sender)
{
    ToyShopConnection->Connected = true;
}
//---------------------------------------------------------------------------

