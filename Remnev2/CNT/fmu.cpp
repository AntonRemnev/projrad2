//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "ClientClassesUnit1.h"
#include "dmu.h"
#include "fruItem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buProductsClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiProducts->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buOrdersClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiOrders->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void Tfm::ShowProducts(){
	dm->cdsProducts->First();

	while(!dm->cdsProducts->Eof){
		TfrItem *x = new TfrItem(sbItems);
		x->Parent = sbItems;
		x->Align = TAlignLayout::Top;
		x->Name = "frItem"+IntToStr(dm->cdsProductsID->Value);
		x->laTitle->Text = dm->cdsProductsTITLE->Value;
		x->laPrice->Text = IntToStr(dm->cdsProductsPRICE->Value) + L"���";
		x->im->Bitmap->Assign(dm->cdsProductsIMAGE);
		x->reClick->Tag = dm->cdsProductsID->Value;
		x->reClick->OnClick = ShowProduct;
		dm->cdsProducts->Next();

	}
	sbItems->RecalcSize();

}


void __fastcall Tfm::ShowProduct(TObject *Sender){
	int aTag = ((TControl *)Sender)->Tag;

	TLocateOptions xLO;

	dm->cdsProducts->Locate(dm->cdsProductsID->FieldName, aTag, xLO);

	imProduct->Bitmap->Assign(dm->cdsProductsIMAGE);
	laProductTitle->Text = dm->cdsProductsTITLE->Value;
	laProductPrice->Text = IntToStr(dm->cdsProductsPRICE->Value) + L"���";
	meProductDescription->Text =  dm->cdsProductsDESCTIPTION->Value;
    buMakeOrder->Tag = aTag;
	tc->GotoVisibleTab(tiProduct->Index);
}
void __fastcall Tfm::FormShow(TObject *Sender)
{
    ShowProducts();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNewOrderClick(TObject *Sender)
{
	edOrderToyTitle->Text = laProductTitle->Text;
	edOrderPrice->Text = IntToStr(dm->cdsProductsPRICE->Value);
    tc->GotoVisibleTab(tiNewOrder->Index);
}
//---------------------------------------------------------------------------


void Tfm::MakeOrder(int aPRODUCT_ID, UnicodeString aPHONE, UnicodeString aEMAIL, UnicodeString aADDRESS, int aPRICE){
   dm->dmDatabaseClient->InsertOrder(aPRODUCT_ID, aPHONE, aEMAIL, aADDRESS, aPRICE);
   ShowMessage("������� �� �����!\n��� �������� �������� � ����!");
   dm->cdsOrders->Refresh();
   tc->GotoVisibleTab(tiMenu->Index);
}
void __fastcall Tfm::buMakeOrderClick(TObject *Sender)
{
	int product_id = buMakeOrder->Tag;
	UnicodeString phone = edOrderPhone->Text;
	UnicodeString email = edOrderEmail->Text;
	UnicodeString address = edOrderAddress->Text;
	int price = StrToInt(edOrderPrice->Text);

	if( phone == "" ||  email == "" ||  address == ""){
        ShowMessage(L"��������� ��� ����!");
		return;
	}

	MakeOrder(product_id, phone, email, address, price);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{

    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buBackClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackProductClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiProduct->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackProductsClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiProducts->Index);
}
//---------------------------------------------------------------------------

