object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object ToyShopConnection: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/')
    AfterConnect = ToyShopConnectionAfterConnect
    Left = 48
    Top = 40
    UniqueId = '{555AF8BD-3876-4DCB-9165-94BC23752181}'
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmDatabase'
    SQLConnection = ToyShopConnection
    Left = 208
    Top = 40
  end
  object cdsProducts: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspProducts'
    RemoteServer = DSProviderConnection1
    Left = 48
    Top = 152
    object cdsProductsID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsProductsTITLE: TWideStringField
      FieldName = 'TITLE'
      Required = True
      Size = 1200
    end
    object cdsProductsPRICE: TIntegerField
      FieldName = 'PRICE'
      Required = True
    end
    object cdsProductsDESCTIPTION: TWideStringField
      FieldName = 'DESCTIPTION'
      Required = True
      Size = 12000
    end
    object cdsProductsIMAGE: TBlobField
      FieldName = 'IMAGE'
      Size = 1
    end
  end
  object cdsOrders: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspOrders'
    RemoteServer = DSProviderConnection1
    Left = 152
    Top = 152
    object cdsOrdersID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsOrdersPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object cdsOrdersPHONE: TWideStringField
      FieldName = 'PHONE'
      Required = True
      Size = 1200
    end
    object cdsOrdersEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Required = True
      Size = 1200
    end
    object cdsOrdersADDRESS: TWideStringField
      FieldName = 'ADDRESS'
      Required = True
      Size = 2000
    end
    object cdsOrdersPRICE: TIntegerField
      FieldName = 'PRICE'
      Required = True
    end
    object cdsOrdersSTATUS: TWideStringField
      FieldName = 'STATUS'
      Required = True
      Size = 1200
    end
  end
end
