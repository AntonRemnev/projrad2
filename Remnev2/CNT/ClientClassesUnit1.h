#ifndef ClientClassesUnit1H
#define ClientClassesUnit1H

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TdmDatabaseClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FToyshopConnectionAfterConnectCommand;
    TDBXCommand *FToyshopConnectionBeforeConnectCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
    TDBXCommand *FInsertOrderCommand;
  public:
    __fastcall TdmDatabaseClient(TDBXConnection *ADBXConnection);
    __fastcall TdmDatabaseClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmDatabaseClient();
    void __fastcall ToyshopConnectionAfterConnect(TJSONObject* Sender);
    void __fastcall ToyshopConnectionBeforeConnect(TJSONObject* Sender);
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
    void __fastcall InsertOrder(int aPRODUCT_ID, System::UnicodeString aPHONE, System::UnicodeString aEMAIL, System::UnicodeString aADDRESS, int aPRICE);
  };

#endif
