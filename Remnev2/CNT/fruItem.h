//---------------------------------------------------------------------------

#ifndef fruItemH
#define fruItemH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfrItem : public TFrame
{
__published:	// IDE-managed Components
	TImage *im;
	TRectangle *Rectangle1;
	TLabel *laTitle;
	TLabel *laPrice;
	TRectangle *reClick;
private:	// User declarations
public:		// User declarations
	__fastcall TfrItem(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrItem *frItem;
//---------------------------------------------------------------------------
#endif
