//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Layers3D.hpp>
#include <FMX.MaterialSources.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.Types.hpp>
#include <FMX.Types3D.hpp>
#include <FMX.Viewport3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TStyleBook *StyleBook1;
	TViewport3D *Viewport3D1;
	TSphere *Sphere1;
	TLightMaterialSource *LightMaterialSource1;
	TLight *Light1;
	TImage3D *Image3D1;
	TSphere *Sphere2;
	TDummy *Dummy1;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TLightMaterialSource *LightMaterialSource2;
	void __fastcall FloatAnimation2Finish(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
