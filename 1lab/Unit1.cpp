//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "dm1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
    tc->ActiveTab= tiList;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	if (sbEdit->IsPressed){
		dm->taList->Edit();
		tc->GotoVisibleTab(tiItem->Index);
		}
	else {
		dm->taList->Edit();
		dm->taListCheckmark->Value= ! dm->taListCheckmark->Value;
		dm->taList->Post();
		lv->Resize();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAddClick(TObject *Sender)
{
	dm->taList->Append();
     tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buCancelClick(TObject *Sender)
{
	dm->taList->Cancel();
	lv->Resize();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buBackClick(TObject *Sender)
{
	buCancelClick(this);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buDelClick(TObject *Sender)
{
	dm->taList->Delete();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buSaveClick(TObject *Sender)
{
	dm->taList->Post();
    lv->Resize();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buImagePrevClick(TObject *Sender)
{
	Glyph->ImageIndex=((int)Glyph->ImageIndex<=0) ?
        dm->il->Count -1 : (int)Glyph->ImageIndex - 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buImageNextClick(TObject *Sender)
{
	Glyph->ImageIndex =(int)Glyph->ImageIndex >= dm->il->Count -1 ?
		0 : (int)Glyph->ImageIndex +1 ;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buImageClearClick(TObject *Sender)
{
    Glyph->ImageIndex= -1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GlyphChanged(TObject *Sender)
{
	if(dm->taList->State == TDataSetState::dsInsert ||
		dm->taList->State == TDataSetState::dsEdit) {
			dm->taListImageIndex->Value = Glyph->ImageIndex;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
   tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lbiDeleteAllClick(TObject *Sender)
{
	dm->taList->First() ;
	while (dm->taList->Eof)   {
		dm->taList->Fields->Clear();
	}
	lv->Items->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lbiAboutClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiHelp->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lbiHelpClick(TObject *Sender)
{
	 tc->GotoVisibleTab(tiAbout->Index);
}
//---------------------------------------------------------------------------

