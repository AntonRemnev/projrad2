//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.MultiView.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.ActnList.hpp>
#include <System.Actions.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiItem;
	TTabItem *tiList;
	TListView *lv;
	TMultiView *mvPopup;
	TListBox *lbPopup;
	TListBoxItem *lbiAbout;
	TListBoxItem *lbiDeleteAll;
	TListBoxItem *lbiHelp;
	TToolBar *tbList;
	TButton *buPopup;
	TButton *buAdd;
	TSpeedButton *sbCheck;
	TSpeedButton *sbEdit;
	TLabel *laCaption;
	TGridPanelLayout *GridPanelLayout1;
	TScrollBox *ScrollBox1;
	TToolBar *tbItem;
	TButton *buDel;
	TButton *buBack;
	TLabel *Label1;
	TButton *buSave;
	TButton *buCancel;
	TComboBox *cbHeadText;
	TGridPanelLayout *GridPanelLayout2;
	TLayout *Layout1;
	TGlyph *Glyph;
	TLabel *Label2;
	TButton *buImagePrev;
	TButton *buImageClear;
	TButton *buImageNext;
	TLabel *Label3;
	TEdit *edText;
	TLabel *Label4;
	TLabel *Label5;
	TMemo *meDetail;
	TSwitch *swCheckmark;
	TLabel *Label6;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkFillControlToField *LinkFillControlToField1;
	TLinkControlToField *LinkControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldImageIndex;
	TLinkControlToField *LinkControlToField2;
	TLinkControlToField *LinkControlToField3;
	TLinkListControlToField *LinkListControlToField1;
	TTabItem *tiAbout;
	TTabItem *tiHelp;
	TToolBar *ToolBar1;
	TToolBar *ToolBar2;
	TButton *Button1;
	TButton *Button2;
	TActionList *ActionList1;
	TLabel *Label7;
	TLabel *Label8;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall buCancelClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buSaveClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImageClearClick(TObject *Sender);
	void __fastcall GlyphChanged(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall lbiDeleteAllClick(TObject *Sender);
	void __fastcall lbiAboutClick(TObject *Sender);
	void __fastcall lbiHelpClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
