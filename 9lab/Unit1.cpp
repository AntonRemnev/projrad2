//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormShow(TObject *Sender)
{
	edName->Text = L"������ " + IntToStr(Random(92));
	edValue->Text = L"����!";
	ttm->AutoConnect();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttmPairedFromLocal(TObject * const Sender, const TTetheringManagerInfo &AManagerInfo)

{
    me->Lines->Add("PairedFromLocal: " +
	AManagerInfo.ManagerText + " - " + AManagerInfo.ManagerIdentifier);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttmPairedToRemote(TObject * const Sender, const TTetheringManagerInfo &AManagerInfo)

{
    me->Lines->Add("PairedToRemote: " +
	AManagerInfo.ManagerText + " - " + AManagerInfo.ManagerIdentifier);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSendClick(TObject *Sender)
{
	for (int i = 0; i < ttp->ConnectedProfiles->Count; i++) {
		ttp->SendString(ttp->ConnectedProfiles->Items[i], edName->Text, edValue->Text);

	}
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource)

{
	me->Lines->Add(AResource->Hint + ": " + AResource->Value.AsString);
}
//---------------------------------------------------------------------------
