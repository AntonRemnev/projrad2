// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "main.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TMainForm *MainForm;

// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::buExitClick(TObject *Sender) {
	Close();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::buPlayClick(TObject *Sender) {
	tc->GotoVisibleTab(tiMain->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormCreate(TObject *Sender) {
	tc->ActiveTab = tiMenu;
	ActiveObject = NULL;
	RecList = new TList();
	currentTag = 1; // �� ��������� �������� - �������
	UpdateButtons();
	buSetRotate->TextSettings->Font->Style = buSetRotate->TextSettings->Font->Style << TFontStyle::fsBold;

}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::Rectangle3DClick(TObject *Sender) {

	if (ActiveObject) {
		ActiveObject->MaterialSource = (TMaterialSource*)PrevMaterial;
		// ���������� �������� �������� �������� �������
		ActiveObject->MaterialBackSource = (TMaterialSource*)PrevMaterial;
		ActiveObject->MaterialShaftSource = (TMaterialSource*)PrevMaterial;
		ActiveObject->Repaint();
	}
	ActiveObject = (TRectangle3D*)Sender;
	if ((TLightMaterialSource*)ActiveObject->MaterialSource != ActiveMaterial) {
		ActiveObject->MaterialSource = (TMaterialSource*)ActiveMaterial;
		ActiveObject->MaterialBackSource = (TMaterialSource*)ActiveMaterial;
		ActiveObject->MaterialShaftSource = (TMaterialSource*)ActiveMaterial;
		ActiveObject->Repaint();
	}

	ShowCoords();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::buAddNewClick(TObject *Sender) {
	if (ActiveObject) {
		ActiveObject->MaterialSource = (TMaterialSource*)PrevMaterial;
		// ���������� �������� �������� �������� �������
		ActiveObject->MaterialBackSource = (TMaterialSource*)PrevMaterial;
		ActiveObject->MaterialShaftSource = (TMaterialSource*)PrevMaterial;
		ActiveObject->Repaint();
	}

	TRectangle3D* newRec = new TRectangle3D(this);

	newRec->Parent = this->Grid3D1;
	newRec->MaterialSource = (TMaterialSource*)ActiveMaterial;
	newRec->MaterialBackSource = (TMaterialSource*)ActiveMaterial;
	newRec->MaterialShaftSource = (TMaterialSource*)ActiveMaterial;
	newRec->OnClick = &Rectangle3DClick;
	ActiveObject = newRec;
	newRec->Repaint();
	RecList->Add(newRec);
	ShowCoords();
	ChangeControlButtonsState(true);
}

// ---------------------------------------------------------------------------





void __fastcall TMainForm::buSetOperationClick(TObject *Sender) {

	if (ActiveObject) {
		currentTag = ((TButton*)Sender)->Tag;
		UpdateButtons();
		switch (currentTag) {
		  case 1:
			buSetRotate->TextSettings->Font->Style = buSetRotate->TextSettings->Font->Style << TFontStyle::fsBold;
			break;

		  case 2:
			buSetScale->TextSettings->Font->Style = buSetScale->TextSettings->Font->Style << TFontStyle::fsBold;
			break;

		  case 3:
			buSetTransform->TextSettings->Font->Style = buSetTransform->TextSettings->Font->Style << TFontStyle::fsBold;
			break;
		}

		ShowCoords();
	}
	else {
		ShowMessage("�������� ������!");
	}
}
// --------------------------------------------------------------------------

void TMainForm::ShowCoords() {
	edPosX->Text = FloatToStr(ActiveObject->Position->X);
	edPosY->Text = FloatToStr(ActiveObject->Position->Y);
	edPosZ->Text = FloatToStr(ActiveObject->Position->Z);

	edRotX->Text = FloatToStr(ActiveObject->RotationAngle->X);
	edRotY->Text = FloatToStr(ActiveObject->RotationAngle->Y);
	edRotZ->Text = FloatToStr(ActiveObject->RotationAngle->Z);

	edScaleX->Text = FloatToStr(ActiveObject->Scale->X);
	edScaleY->Text = FloatToStr(ActiveObject->Scale->Y);
	edScaleZ->Text = FloatToStr(ActiveObject->Scale->Z);
}

void __fastcall TMainForm::buAddDuplicateClick(TObject *Sender) {
	if (ActiveObject) {
		ActiveObject->MaterialSource = (TMaterialSource*)PrevMaterial;
		// ���������� �������� �������� �������� �������
		ActiveObject->MaterialBackSource = (TMaterialSource*)PrevMaterial;
		ActiveObject->MaterialShaftSource = (TMaterialSource*)PrevMaterial;
		ActiveObject->Repaint();
	}
	else {
		ShowMessage("������ �� ������!");
		return;
	}

	TRectangle3D* newRec = new TRectangle3D(this);

	newRec->Parent = this->Grid3D1;
	newRec->MaterialSource = (TMaterialSource*)ActiveMaterial;
	newRec->MaterialBackSource = (TMaterialSource*)ActiveMaterial;
	newRec->MaterialShaftSource = (TMaterialSource*)ActiveMaterial;
	newRec->OnClick = &Rectangle3DClick;

	newRec->Position->X = ActiveObject->Position->X;
	newRec->Position->Y = ActiveObject->Position->Y;
	newRec->Position->Z = ActiveObject->Position->Z;

	newRec->RotationAngle->X = ActiveObject->RotationAngle->X;
	newRec->RotationAngle->Y = ActiveObject->RotationAngle->Y;
	newRec->RotationAngle->Z = ActiveObject->RotationAngle->Z;

	newRec->Scale->X = ActiveObject->Scale->X;
	newRec->Scale->Y = ActiveObject->Scale->Y;
	newRec->Scale->Z = ActiveObject->Scale->Z;

	ActiveObject = newRec;
	newRec->Repaint();

	RecList->Add(newRec);
	ShowCoords();
}

// ---------------------------------------------------------------------------


void __fastcall TMainForm::buDeleteClick(TObject *Sender) {
	if (!ActiveObject) {
		ShowMessage("������ �� ������!");
		return;
	}
	ActiveObject->Free();
	ActiveObject = NULL;
	Viewport3D1->Repaint();
    ChangeControlButtonsState(false);
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::buBackClick(TObject *Sender) {
	tc->GotoVisibleTab(tiMenu->Index);
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::edPosXChange(TObject *Sender)
{
	ActiveObject->Position->X = StrToFloat(edPosX->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edPosYChange(TObject *Sender)
{
    ActiveObject->Position->Y = StrToFloat(edPosY->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edPosZChange(TObject *Sender)
{
    ActiveObject->Position->Z = StrToFloat(edPosZ->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edRotXChange(TObject *Sender)
{
    ActiveObject->RotationAngle->X = StrToFloat(edRotX->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edRotYChange(TObject *Sender)
{
    ActiveObject->RotationAngle->Y = StrToFloat(edRotY->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edRotZChange(TObject *Sender)
{
    ActiveObject->RotationAngle->Z = StrToFloat(edRotZ->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edScaleXChange(TObject *Sender)
{
	ActiveObject->Scale->X = StrToFloat(edScaleX->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edScaleYChange(TObject *Sender)
{
	ActiveObject->Scale->Y = StrToFloat(edScaleY->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::edScaleZChange(TObject *Sender)
{
    ActiveObject->Scale->Z = StrToFloat(edScaleZ->Text);
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta,
          bool &Handled)
{
	if(tc->ActiveTab == tiMain){
		if(WheelDelta < 0){
			Camera1->Position->Y--;
            Camera1->Position->Z--;
		}else{
			Camera1->Position->Y++;
			Camera1->Position->Z++;
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::tbXChange(TObject *Sender)
{

	int tag = currentTag; // 1 - �������; 2 - ������; 3 - �����������
	switch (tag) {
	case 1:
		ActiveObject->RotationAngle->X +=  tbX->Value/10;
		break;
	case 2:
		ActiveObject->Scale->X +=  tbX->Value/100;
		break;
	case 3:
		ActiveObject->Position->X +=  tbX->Value/100;
		break;
	default:
		ShowMessage("Invalid tag!");
		break;
	}

	tbX->Value = 0;
	ShowCoords();

}
//---------------------------------------------------------------------------



void __fastcall TMainForm::tbYChange(TObject *Sender)
{

	int tag = currentTag; // 1 - �������; 2 - ������; 3 - �����������
	switch (tag) {
	case 1:
		ActiveObject->RotationAngle->Y +=  tbY->Value/10;
		break;
	case 2:
		ActiveObject->Scale->Y +=  tbY->Value/100;
		break;
	case 3:
		ActiveObject->Position->Y +=  tbY->Value/100;
		break;
	default:
		ShowMessage("Invalid tag!");
		break;
	}
	tbY->Value = 0;
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::tbZChange(TObject *Sender)
{
    int tag = currentTag; // 1 - �������; 2 - ������; 3 - �����������
	switch (tag) {
	case 1:
		ActiveObject->RotationAngle->Z +=  tbZ->Value/10;
		break;
	case 2:
		ActiveObject->Scale->Z +=  tbZ->Value/100;
		break;
	case 3:
		ActiveObject->Position->Z +=  tbZ->Value/100;
		break;
	default:
		ShowMessage("Invalid tag!");
		break;
	}
	tbZ->Value = 0;
	ShowCoords();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::buLMouseControlMoveMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, float X, float Y)
{
	isMouseControlDown = true;
	FX = X;
	FY = Y;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::buLMouseControlMoveMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
    isMouseControlDown = false;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::buLMouseControlMoveMouseMove(TObject *Sender, TShiftState Shift, float X,
          float Y)
{
	if(isMouseControlDown){
		float conPosX = lMouseControl->Position->X;
		float conPosY = lMouseControl->Position->Y;
		lMouseControl->Position->X += X - FX;
		lMouseControl->Position->Y += Y - FY;


	}
}
//---------------------------------------------------------------------------
void TMainForm::UpdateButtons(){
	buSetRotate->TextSettings->Font->Style = buSetRotate->TextSettings->Font->Style >> TFontStyle::fsBold;
	buSetTransform->TextSettings->Font->Style = buSetTransform->TextSettings->Font->Style >> TFontStyle::fsBold;
	buSetScale->TextSettings->Font->Style = buSetScale->TextSettings->Font->Style >> TFontStyle::fsBold;
}


void TMainForm::ChangeControlButtonsState(bool isEnabled){
	lControlPosition->Enabled = isEnabled;
	lControlRotation->Enabled = isEnabled;
	lControlScale->Enabled = isEnabled;
	lMouseControlTrackBars->Enabled = isEnabled;
	buDelete->Enabled = isEnabled;
	buAddDuplicate->Enabled = isEnabled;
	lControlAction->Enabled = isEnabled;

}

void __fastcall TMainForm::tbCameraRotChange(TObject *Sender)
{
    Grid3D1->RotationAngle->Z = tbCameraRot->Value/10;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button1Click(TObject *Sender)
{
	ShowMessage(L"������ ���������� ���������� ��������� ����� ���������� ��� ������ ��������� ������� (�������������� �������)\n\n������ ���������� ������� � ������������ ����� ���������� � ������������\n\n������� �������� ��������� �����");
}
//---------------------------------------------------------------------------

