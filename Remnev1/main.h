//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Objects3D.hpp>
#include <FMX.Viewport3D.hpp>
#include <System.Math.Vectors.hpp>
#include <FMX.Edit.hpp>
#include <FMX.MaterialSources.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiMain;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buPlay;
	TButton *buExit;
	TToolBar *ToolBar1;
	TButton *buBack;
	TViewport3D *Viewport3D1;
	TGrid3D *Grid3D1;
	TButton *buSetRotate;
	TButton *buSetScale;
	TButton *buSetTransform;
	TButton *buAddDuplicate;
	TLightMaterialSource *ActiveMaterial;
	TLightMaterialSource *PrevMaterial;
	TButton *buAddNew;
	TFloatAnimation *faRotateCamera;
	TLabel *Label2;
	TLabel *Label3;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TFloatAnimation *FloatAnimation3;
	TButton *buDelete;
	TLayout *lControl;
	TLabel *Label5;
	TLayout *lControlPosition;
	TLabel *Label6;
	TLayout *Layout4;
	TLabel *Label7;
	TEdit *edPosX;
	TLayout *Layout5;
	TLabel *Label8;
	TEdit *edPosZ;
	TLayout *Layout6;
	TLabel *Label9;
	TEdit *edPosY;
	TLayout *lControlRotation;
	TLabel *Label10;
	TLayout *Layout3;
	TLabel *Label11;
	TEdit *edRotX;
	TLayout *Layout7;
	TLabel *Label12;
	TEdit *edRotZ;
	TLayout *Layout8;
	TLabel *Label13;
	TEdit *edRotY;
	TLayout *lControlScale;
	TLabel *Label14;
	TLayout *Layout9;
	TLabel *Label15;
	TEdit *edScaleX;
	TLayout *Layout10;
	TLabel *Label16;
	TEdit *edScaleZ;
	TLayout *Layout11;
	TLabel *Label17;
	TEdit *edScaleY;
	TLabel *Label18;
	TLayout *lControlAction;
	TCamera *Camera1;
	TLayout *lControlNew;
	TLayout *lMouseControl;
	TTrackBar *tbX;
	TLabel *Label19;
	TLayout *lMouseX;
	TLayout *lMouseY;
	TTrackBar *tbY;
	TLabel *Label20;
	TLayout *lMouseZ;
	TTrackBar *tbZ;
	TLabel *Label21;
	TLabel *Label1;
	TButton *buLMouseControlMove;
	TLayout *lMouseTitle;
	TLabel *Label22;
	TLayout *lCameraControl;
	TTrackBar *tbCameraRot;
	TLayout *lMouseControlTrackBars;
	TLabel *Label4;
	TButton *Button1;
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Rectangle3DClick(TObject *Sender);
	void __fastcall buAddNewClick(TObject *Sender);
	void __fastcall buSetOperationClick(TObject *Sender);
	void __fastcall buAddDuplicateClick(TObject *Sender);
	void __fastcall buDeleteClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall edPosXChange(TObject *Sender);
	void __fastcall edPosYChange(TObject *Sender);
	void __fastcall edPosZChange(TObject *Sender);
	void __fastcall edRotXChange(TObject *Sender);
	void __fastcall edRotYChange(TObject *Sender);
	void __fastcall edRotZChange(TObject *Sender);
	void __fastcall edScaleXChange(TObject *Sender);
	void __fastcall edScaleYChange(TObject *Sender);
	void __fastcall edScaleZChange(TObject *Sender);
	void __fastcall FormMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta,
          bool &Handled);
	void __fastcall tbXChange(TObject *Sender);
	void __fastcall tbYChange(TObject *Sender);
	void __fastcall tbZChange(TObject *Sender);
	void __fastcall buLMouseControlMoveMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buLMouseControlMoveMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall buLMouseControlMoveMouseMove(TObject *Sender, TShiftState Shift, float X, float Y);
	void __fastcall tbCameraRotChange(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);


private:	// User declarations
	TRectangle3D* ActiveObject;
	TList* RecList;
	int currentTag;
	bool isMouseDown;
	bool isMouseControlDown;
	bool isMouseCameraDown;
	float FX, FY;


	void ShowCoords();
	void UpdateButtons();
    void ChangeControlButtonsState(bool isEnabled);

public:		// User declarations
	__fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
