#ifndef ClientClassesUnit1H
#define ClientClassesUnit1H

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TServerMethods1Client : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FSweetlifeConnectionBeforeConnectCommand;
    TDBXCommand *FSweetlifeConnectionAfterConnectCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
    TDBXCommand *FNewOrderCommand;
    TDBXCommand *FNewReviewCommand;
  public:
    __fastcall TServerMethods1Client(TDBXConnection *ADBXConnection);
    __fastcall TServerMethods1Client(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TServerMethods1Client();
    void __fastcall SweetlifeConnectionBeforeConnect(TJSONObject* Sender);
    void __fastcall SweetlifeConnectionAfterConnect(TJSONObject* Sender);
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
    void __fastcall NewOrder(int aProductID, double aDate, System::UnicodeString aFIO, System::UnicodeString aPhone, System::UnicodeString aNotes);
    void __fastcall NewReview(int aProductID, System::UnicodeString aComment);
  };

#endif
