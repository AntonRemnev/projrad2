//----------------------------------------------------------------------------

#ifndef ClientModuleUnit1H
#define ClientModuleUnit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "ClientClassesUnit1.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
//----------------------------------------------------------------------------
class TClientModule1 : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TClientDataSet *ClientDataSetOrder;
	TDSProviderConnection *DSProviderConnection;
	TClientDataSet *ClientDataSetProduct;
	TIntegerField *ClientDataSetOrderID;
	TIntegerField *ClientDataSetOrderPRODUCT_ID;
	TDateField *ClientDataSetOrderDATE;
	TWideStringField *ClientDataSetOrderFIO;
	TWideStringField *ClientDataSetOrderPHONE;
	TWideStringField *ClientDataSetOrderNOTES;
	TIntegerField *ClientDataSetOrderSTATUS;
	TIntegerField *ClientDataSetProductID;
	TWideStringField *ClientDataSetProductNAME;
	TWideStringField *ClientDataSetProductNOTES;
	TSingleField *ClientDataSetProductWEIGHT;
	TSingleField *ClientDataSetProductPRICE;
	TBlobField *ClientDataSetProductIMAGE;
	TStringField *ClientDataSetOrderProductName;
	TClientDataSet *ClientDataSetReviews;
	TIntegerField *ClientDataSetReviewsID;
	TIntegerField *ClientDataSetReviewsPRODUCT_ID;
	TWideStringField *ClientDataSetReviewsCOMMENT;
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TServerMethods1Client* FServerMethods1Client;
	TServerMethods1Client* GetServerMethods1Client(void);
public:		// User declarations
	__fastcall TClientModule1(TComponent* Owner);
	__fastcall ~TClientModule1();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TServerMethods1Client* ServerMethods1Client = {read=GetServerMethods1Client, write=FServerMethods1Client};
};
//---------------------------------------------------------------------------
extern PACKAGE TClientModule1 *ClientModule1;
//---------------------------------------------------------------------------
#endif
