object ClientModule1: TClientModule1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      'Filters={}')
    Connected = True
    Left = 72
    Top = 64
    UniqueId = '{23F2C925-E90B-4908-8C71-D502DEE40A2B}'
  end
  object ClientDataSetOrder: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderOrder'
    RemoteServer = DSProviderConnection
    Left = 64
    Top = 168
    object ClientDataSetOrderID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object ClientDataSetOrderPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object ClientDataSetOrderDATE: TDateField
      FieldName = 'DATE'
      Required = True
    end
    object ClientDataSetOrderFIO: TWideStringField
      FieldName = 'FIO'
      Required = True
      Size = 280
    end
    object ClientDataSetOrderPHONE: TWideStringField
      FieldName = 'PHONE'
      Required = True
      Size = 80
    end
    object ClientDataSetOrderNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 1020
    end
    object ClientDataSetOrderSTATUS: TIntegerField
      FieldName = 'STATUS'
    end
    object ClientDataSetOrderProductName: TStringField
      FieldKind = fkLookup
      FieldName = 'ProductName'
      LookupDataSet = ClientDataSetProduct
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'PRODUCT_ID'
      Lookup = True
    end
  end
  object DSProviderConnection: TDSProviderConnection
    ServerClassName = 'TServerMethods1'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 152
    Top = 56
  end
  object ClientDataSetProduct: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderProduct'
    RemoteServer = DSProviderConnection
    Left = 192
    Top = 176
    object ClientDataSetProductID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object ClientDataSetProductNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 240
    end
    object ClientDataSetProductNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 1020
    end
    object ClientDataSetProductWEIGHT: TSingleField
      FieldName = 'WEIGHT'
    end
    object ClientDataSetProductPRICE: TSingleField
      FieldName = 'PRICE'
      Required = True
    end
    object ClientDataSetProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Size = 1
    end
  end
  object ClientDataSetReviews: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderReviews'
    RemoteServer = DSProviderConnection
    Left = 328
    Top = 184
    object ClientDataSetReviewsID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object ClientDataSetReviewsPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object ClientDataSetReviewsCOMMENT: TWideStringField
      FieldName = 'COMMENT'
      Required = True
      Size = 4000
    end
  end
end
