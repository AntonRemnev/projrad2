//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "ClientModuleUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvCakesItemClick(TObject * const Sender, TListViewItem * const AItem)

{
    tc->GotoVisibleTab(TabItem3->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->GotoVisibleTab(TabItem2->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button5Click(TObject *Sender)
{
	tc->GotoVisibleTab(TabItem2->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button6Click(TObject *Sender)
{
	tc->GotoVisibleTab(TabItem2->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tcChange(TObject *Sender)
{
	if(tc->ActiveTab == tiNewOrder)
	{
		edNewOrderProductName->Text = ClientModule1->ClientDataSetProductNAME->AsString;
		edNewOrderDate->Date = Now();
		meNewOrderNotes->Lines->Clear();
    }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buNewOrderClick(TObject *Sender)
{
	ClientModule1->ServerMethods1Client->NewOrder(
		ClientModule1->ClientDataSetProductID->Value,
		edNewOrderDate->Date,
		edNewOrderFIO->Text,
		edNewOrderPhone->Text,
		meNewOrderNotes->Text
	);
	ClientModule1->ClientDataSetOrder->Refresh();
	ShowMessage(L"����� ���������");
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button4Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button7Click(TObject *Sender)
{
    tc->GotoVisibleTab(tiNewOrder->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button8Click(TObject *Sender)
{
	tc->GotoVisibleTab(TabItem3->Index);
}
//---------------------------------------------------------------------------


void __fastcall Tfm::Button12Click(TObject *Sender)
{
	ClientModule1->ClientDataSetReviews->Filter =
		ClientModule1->ClientDataSetReviewsPRODUCT_ID->AsString + " = " +
		ClientModule1->ClientDataSetProductID->Value;
	tc->GotoVisibleTab(TabItem4->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button13Click(TObject *Sender)
{
	tc->GotoVisibleTab(TabItem5->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Button15Click(TObject *Sender)
{
	ShowMessage(ClientModule1->ClientDataSetProductID->Value);
	ClientModule1->ServerMethods1Client->NewReview(
		ClientModule1->ClientDataSetProductID->Value,
		mReview->Text
	);
	ClientModule1->ClientDataSetReviews->Refresh();
	ShowMessage(L"����� ��������");
	tc->GotoVisibleTab(TabItem5->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab=TabItem1;
}
//---------------------------------------------------------------------------

