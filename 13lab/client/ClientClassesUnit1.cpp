//
// Created by the DataSnap proxy generator.
// 18.12.2017 9:57:48
// 

#include "ClientClassesUnit1.h"

void __fastcall TServerMethods1Client::SweetlifeConnectionBeforeConnect(TJSONObject* Sender)
{
  if (FSweetlifeConnectionBeforeConnectCommand == NULL)
  {
    FSweetlifeConnectionBeforeConnectCommand = FDBXConnection->CreateCommand();
    FSweetlifeConnectionBeforeConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FSweetlifeConnectionBeforeConnectCommand->Text = "TServerMethods1.SweetlifeConnectionBeforeConnect";
    FSweetlifeConnectionBeforeConnectCommand->Prepare();
  }
  FSweetlifeConnectionBeforeConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FSweetlifeConnectionBeforeConnectCommand->ExecuteUpdate();
}

void __fastcall TServerMethods1Client::SweetlifeConnectionAfterConnect(TJSONObject* Sender)
{
  if (FSweetlifeConnectionAfterConnectCommand == NULL)
  {
    FSweetlifeConnectionAfterConnectCommand = FDBXConnection->CreateCommand();
    FSweetlifeConnectionAfterConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FSweetlifeConnectionAfterConnectCommand->Text = "TServerMethods1.SweetlifeConnectionAfterConnect";
    FSweetlifeConnectionAfterConnectCommand->Prepare();
  }
  FSweetlifeConnectionAfterConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FSweetlifeConnectionAfterConnectCommand->ExecuteUpdate();
}

System::UnicodeString __fastcall TServerMethods1Client::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TServerMethods1.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TServerMethods1Client::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TServerMethods1.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TServerMethods1Client::NewOrder(int aProductID, double aDate, System::UnicodeString aFIO, System::UnicodeString aPhone, System::UnicodeString aNotes)
{
  if (FNewOrderCommand == NULL)
  {
    FNewOrderCommand = FDBXConnection->CreateCommand();
    FNewOrderCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FNewOrderCommand->Text = "TServerMethods1.NewOrder";
    FNewOrderCommand->Prepare();
  }
  FNewOrderCommand->Parameters->Parameter[0]->Value->SetInt32(aProductID);
  FNewOrderCommand->Parameters->Parameter[1]->Value->SetDouble(aDate);
  FNewOrderCommand->Parameters->Parameter[2]->Value->SetWideString(aFIO);
  FNewOrderCommand->Parameters->Parameter[3]->Value->SetWideString(aPhone);
  FNewOrderCommand->Parameters->Parameter[4]->Value->SetWideString(aNotes);
  FNewOrderCommand->ExecuteUpdate();
}

void __fastcall TServerMethods1Client::NewReview(int aProductID, System::UnicodeString aComment)
{
  if (FNewReviewCommand == NULL)
  {
    FNewReviewCommand = FDBXConnection->CreateCommand();
    FNewReviewCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FNewReviewCommand->Text = "TServerMethods1.NewReview";
    FNewReviewCommand->Prepare();
  }
  FNewReviewCommand->Parameters->Parameter[0]->Value->SetInt32(aProductID);
  FNewReviewCommand->Parameters->Parameter[1]->Value->SetWideString(aComment);
  FNewReviewCommand->ExecuteUpdate();
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TServerMethods1Client::TServerMethods1Client(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TServerMethods1Client::~TServerMethods1Client()
{
  delete FSweetlifeConnectionBeforeConnectCommand;
  delete FSweetlifeConnectionAfterConnectCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
  delete FNewOrderCommand;
  delete FNewReviewCommand;
}

