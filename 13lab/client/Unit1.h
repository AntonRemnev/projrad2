//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.DateTimeCtrls.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TImage *Image1;
	TButton *Button1;
	TLayout *Layout1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TTabControl *tc;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TToolBar *ToolBar1;
	TLabel *Label2;
	TButton *Button5;
	TListView *lvCakes;
	TToolBar *ToolBar2;
	TButton *Button6;
	TButton *Button7;
	TLabel *Label3;
	TLayout *Layout2;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TImage *Image2;
	TLabel *Label7;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TTabItem *tiNewOrder;
	TToolBar *ToolBar3;
	TButton *Button8;
	TButton *buNewOrder;
	TListBox *ListBox2;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TListBoxItem *ListBoxItem1;
	TEdit *edNewOrderProductName;
	TEdit *edNewOrderFIO;
	TEdit *edNewOrderPhone;
	TLabel *Label8;
	TTabItem *tiList;
	TToolBar *ToolBar4;
	TButton *Button10;
	TButton *Button11;
	TListView *lvOrders;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TLinkControlToField *LinkControlToField1;
	TDateEdit *edNewOrderDate;
	TMemo *meNewOrderNotes;
	TTabItem *TabItem4;
	TToolBar *ToolBar5;
	TLabel *Label9;
	TButton *Button9;
	TListView *ListView1;
	TBindSourceDB *BindSourceDB3;
	TLinkListControlToField *LinkListControlToField3;
	TButton *Button12;
	TButton *Button13;
	TTabItem *TabItem5;
	TToolBar *ToolBar6;
	TLabel *Label10;
	TButton *Button14;
	TLabel *Label11;
	TMemo *mReview;
	TButton *Button15;
	void __fastcall lvCakesItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall tcChange(TObject *Sender);
	void __fastcall buNewOrderClick(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button15Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
