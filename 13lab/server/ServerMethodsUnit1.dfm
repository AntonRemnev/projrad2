object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 296
  Width = 399
  object SweetlifeConnection: TSQLConnection
    ConnectionName = 'SweetLife'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=C:\Users\VD\Desktop\Lab_SweetLife\SWEETLIFE.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    AfterConnect = SweetlifeConnectionAfterConnect
    BeforeConnect = SweetlifeConnectionBeforeConnect
    Connected = True
    Left = 42
    Top = 27
  end
  object taOrderList: TSQLDataSet
    Active = True
    CommandText = 'ORDER_LIST'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SweetlifeConnection
    Left = 42
    Top = 75
    object taOrderListID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taOrderListPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object taOrderListDATE: TDateField
      FieldName = 'DATE'
      Required = True
    end
    object taOrderListFIO: TWideStringField
      FieldName = 'FIO'
      Required = True
      Size = 280
    end
    object taOrderListPHONE: TWideStringField
      FieldName = 'PHONE'
      Required = True
      Size = 80
    end
    object taOrderListNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 1020
    end
    object taOrderListSTATUS: TIntegerField
      FieldName = 'STATUS'
    end
  end
  object taProduct: TSQLDataSet
    Active = True
    CommandText = 'PRODUCT'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = SweetlifeConnection
    Left = 120
    Top = 71
    object taProductID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taProductNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 240
    end
    object taProductNOTES: TWideStringField
      FieldName = 'NOTES'
      Size = 1020
    end
    object taProductWEIGHT: TSingleField
      FieldName = 'WEIGHT'
    end
    object taProductPRICE: TSingleField
      FieldName = 'PRICE'
      Required = True
    end
    object taProductIMAGE: TBlobField
      FieldName = 'IMAGE'
      Size = 1
    end
  end
  object DataSetProviderOrder: TDataSetProvider
    DataSet = taOrderList
    Left = 56
    Top = 152
  end
  object DataSetProviderProduct: TDataSetProvider
    DataSet = taProduct
    Left = 168
    Top = 144
  end
  object Order_list_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'PRODUCT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftDate
        Precision = 4
        Name = 'DATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 70
        Name = 'FIO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 20
        Name = 'PHONE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 255
        Name = 'NOTES'
        ParamType = ptInput
      end>
    SQLConnection = SweetlifeConnection
    StoredProcName = 'ORDER_LIST_INS'
    Left = 128
    Top = 214
  end
  object ReviewsTable: TSQLDataSet
    Active = True
    CommandText = 'REVIEWS'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SweetlifeConnection
    Left = 190
    Top = 74
    object ReviewsTableID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object ReviewsTablePRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Required = True
    end
    object ReviewsTableCOMMENT: TWideStringField
      FieldName = 'COMMENT'
      Required = True
      Size = 4000
    end
  end
  object Reviews_insProc: TSQLStoredProc
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Precision = 4
        Name = 'PRODUCT_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Precision = 1000
        Name = 'COMMENT'
        ParamType = ptInput
      end>
    SQLConnection = SweetlifeConnection
    StoredProcName = 'REVIEWS_INS'
    Left = 242
    Top = 217
  end
  object DataSetProviderReviews: TDataSetProvider
    DataSet = ReviewsTable
    Left = 280
    Top = 152
  end
end
