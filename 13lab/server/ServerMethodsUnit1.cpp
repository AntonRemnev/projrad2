//---------------------------------------------------------------------------

#pragma hdrstop

#include "ServerMethodsUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TServerMethods1::TServerMethods1(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TServerMethods1::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------

void __fastcall TServerMethods1::SweetlifeConnectionBeforeConnect(TObject *Sender)
{
	SweetlifeConnection->Params->Values["DataBase"] = "..\\..\\..\\SWEETLIFE.FDB";
}
//---------------------------------------------------------------------------
void __fastcall TServerMethods1::SweetlifeConnectionAfterConnect(TObject *Sender)
{
	taProduct->Open();
	taOrderList->Open();
}

void TServerMethods1::NewOrder(int aProductID, double aDate, UnicodeString aFIO,
	UnicodeString aPhone, UnicodeString aNotes)
{
	Order_list_insProc->Params->ParamByName("PRODUCT_ID")->Value = aProductID;
	Order_list_insProc->Params->ParamByName("DATE")->Value = aDate;
	Order_list_insProc->Params->ParamByName("FIO")->Value = aFIO;
	Order_list_insProc->Params->ParamByName("PHONE")->Value = aPhone;
	Order_list_insProc->Params->ParamByName("NOTES")->Value = aNotes;
	Order_list_insProc->ExecProc();
}
void TServerMethods1::NewReview(int aProductID, UnicodeString aComment)
{
	Reviews_insProc->Params->ParamByName("PRODUCT_ID")->Value = aProductID;
	Reviews_insProc->Params->ParamByName("COMMENT")->Value = aComment;
	Reviews_insProc->ExecProc();
}
//---------------------------------------------------------------------------
