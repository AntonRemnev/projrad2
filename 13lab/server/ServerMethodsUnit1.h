//---------------------------------------------------------------------------

#ifndef ServerMethodsUnit1H
#define ServerMethodsUnit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
#include <Data.DB.hpp>
#include <Data.DBXFirebird.hpp>
#include <Data.FMTBcd.hpp>
#include <Data.SqlExpr.hpp>
#include <Datasnap.Provider.hpp>
//---------------------------------------------------------------------------
class TServerMethods1 : public TDSServerModule
{
__published:	// IDE-managed Components
	TSQLConnection *SweetlifeConnection;
	TSQLDataSet *taOrderList;
	TSQLDataSet *taProduct;
	TDataSetProvider *DataSetProviderOrder;
	TDataSetProvider *DataSetProviderProduct;
	TSQLStoredProc *Order_list_insProc;
	TIntegerField *taProductID;
	TWideStringField *taProductNAME;
	TWideStringField *taProductNOTES;
	TSingleField *taProductWEIGHT;
	TSingleField *taProductPRICE;
	TBlobField *taProductIMAGE;
	TIntegerField *taOrderListID;
	TIntegerField *taOrderListPRODUCT_ID;
	TDateField *taOrderListDATE;
	TWideStringField *taOrderListFIO;
	TWideStringField *taOrderListPHONE;
	TWideStringField *taOrderListNOTES;
	TIntegerField *taOrderListSTATUS;
	TSQLDataSet *ReviewsTable;
	TSQLStoredProc *Reviews_insProc;
	TDataSetProvider *DataSetProviderReviews;
	TIntegerField *ReviewsTableID;
	TIntegerField *ReviewsTablePRODUCT_ID;
	TWideStringField *ReviewsTableCOMMENT;
	void __fastcall SweetlifeConnectionBeforeConnect(TObject *Sender);
	void __fastcall SweetlifeConnectionAfterConnect(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TServerMethods1(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
	void NewOrder(int aProductID, double aDate, UnicodeString aFIO,
		UnicodeString aPhone, UnicodeString aNotes);
	void NewReview(int aProductID, UnicodeString aComment);
};
//---------------------------------------------------------------------------
extern PACKAGE TServerMethods1 *ServerMethods1;
//---------------------------------------------------------------------------
#endif

