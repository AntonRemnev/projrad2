object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  Height = 191
  Width = 257
  object BeautysaloonConnection: TSQLConnection
    ConnectionName = 'BEAUTYSALOON'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver250.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver250.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=C:\Users\VD\Desktop\BEAUTYSALOON.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    Connected = True
    Left = 65
    Top = 12
  end
  object CategoryTable: TSQLDataSet
    Active = True
    CommandText = 'CATEGORY'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = BeautysaloonConnection
    Left = 157
    Top = 13
  end
  object ServiceTable: TSQLDataSet
    Active = True
    CommandText = 'SERVICE'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = -1
    Params = <>
    SQLConnection = BeautysaloonConnection
    Left = 154
    Top = 65
  end
  object DataSetProviderCategory: TDataSetProvider
    DataSet = CategoryTable
    Left = 64
    Top = 56
  end
  object DataSetProviderService: TDataSetProvider
    DataSet = ServiceTable
    Left = 64
    Top = 104
  end
end
