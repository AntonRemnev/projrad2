//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "ClientModuleUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->GotoVisibleTab(tcCategory->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tcCategory;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvCategoryItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab(tcService->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvServiceItemClick(TObject * const Sender, TListViewItem * const AItem)

{
    tc->GotoVisibleTab(tcOffer->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bOfferClick(TObject *Sender)
{
	ShowMessage(L"������� �� �����! ��������� �� ������ 8 (999) 000-00-00 ��� ������������� ������.");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
    tc->GotoVisibleTab(tcService->Index);
}
//---------------------------------------------------------------------------
