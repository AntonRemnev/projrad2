object dm: Tdm
  OldCreateOrder = False
  Height = 126
  Width = 302
  object FDConnection: TFDConnection
    Params.Strings = (
      'CharacterSet=UTF8'
      'Database=C:\Users\VD\Desktop\Lab_B_S\BEAUTYSALOON.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 136
    Top = 16
  end
  object FDTableCategory: TFDTable
    AfterScroll = FDTableCategoryAfterScroll
    Filtered = True
    IndexFieldNames = 'ID'
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'CATEGORY'
    TableName = 'CATEGORY'
    Left = 136
    Top = 64
    object FDTableCategoryID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object FDTableCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 50
    end
  end
  object FDTableService: TFDTable
    Filtered = True
    IndexFieldNames = 'ID'
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'SERVICE'
    TableName = 'SERVICE'
    Left = 232
    Top = 64
    object FDTableServiceID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      Required = True
    end
    object FDTableServiceCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object FDTableServiceNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 60
    end
    object FDTableServicePRICE: TSingleField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
    object FDTableServiceDURATION: TIntegerField
      FieldName = 'DURATION'
      Origin = 'DURATION'
    end
  end
  object FDPhysFBDriverLink: TFDPhysFBDriverLink
    Left = 40
    Top = 16
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 40
    Top = 64
  end
end
