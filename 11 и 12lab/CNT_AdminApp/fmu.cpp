//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bConnectClick(TObject *Sender)
{
	dm->OpenDB(eFileName->Text);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bDisconnectClick(TObject *Sender)
{
    dm->FDConnection->Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::bOpenClick(TObject *Sender)
{
	od->InitialDir = Ioutils::TPath::GetDirectoryName(eFileName->Text);
	if(od->Execute())
	{
        eFileName->Text = od->FileName;
    }
}
//---------------------------------------------------------------------------
