//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDTableCategoryAfterScroll(TDataSet *DataSet)
{
	FDTableService->Filter =
		FDTableServiceCATEGORY_ID->FieldName + " = " + FDTableCategoryID->AsString;
}
//---------------------------------------------------------------------------
void Tdm::OpenDB(UnicodeString s)
{
	FDConnection->Close();
	FDConnection->Params->Values["DataBase"] = s;
	FDTableCategory->Open();
	FDTableService->Open();
}