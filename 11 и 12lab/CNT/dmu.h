//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uProxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
//----------------------------------------------------------------------------
class TClientModule1 : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection;
	TDSProviderConnection *DSProviderConnection;
	TClientDataSet *ClientDataSetCategory;
	TClientDataSet *ClientDataSetService;
	TIntegerField *ClientDataSetCategoryID;
	TWideStringField *ClientDataSetCategoryNAME;
	TIntegerField *ClientDataSetServiceID;
	TIntegerField *ClientDataSetServiceCATEGORY_ID;
	TWideStringField *ClientDataSetServiceNAME;
	TSingleField *ClientDataSetServicePRICE;
	TIntegerField *ClientDataSetServiceDURATION;
	void __fastcall ClientDataSetCategoryAfterScroll(TDataSet *DataSet);
	void __fastcall ClientDataSetCategoryAfterPost(TDataSet *DataSet);
	void __fastcall ClientDataSetServiceAfterPost(TDataSet *DataSet);
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TServerMethods1Client* FServerMethods1Client;
	TServerMethods1Client* GetServerMethods1Client(void);
public:		// User declarations
	__fastcall TClientModule1(TComponent* Owner);
	__fastcall ~TClientModule1();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TServerMethods1Client* ServerMethods1Client = {read=GetServerMethods1Client, write=FServerMethods1Client};
};
//---------------------------------------------------------------------------
extern PACKAGE TClientModule1 *ClientModule1;
//---------------------------------------------------------------------------
#endif
