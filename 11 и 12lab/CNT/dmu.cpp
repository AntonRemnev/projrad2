//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TClientModule1 *ClientModule1;
//---------------------------------------------------------------------------
__fastcall TClientModule1::TClientModule1(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall TClientModule1::~TClientModule1()
{
	delete FServerMethods1Client;
}

TServerMethods1Client* TClientModule1::GetServerMethods1Client(void)
{
	if (FServerMethods1Client == NULL)
	{
		SQLConnection->Open();
		FServerMethods1Client = new TServerMethods1Client(SQLConnection->DBXConnection, FInstanceOwner);
	}
	return FServerMethods1Client;
};

void __fastcall TClientModule1::ClientDataSetCategoryAfterScroll(TDataSet *DataSet)

{
	ClientDataSetService->Filter =
        ClientDataSetServiceCATEGORY_ID->FieldName + " = " + ClientDataSetCategoryID->AsString;
}
//---------------------------------------------------------------------------
void __fastcall TClientModule1::ClientDataSetCategoryAfterPost(TDataSet *DataSet)

{
    ClientDataSetCategory->ApplyUpdates(-1);
}
//---------------------------------------------------------------------------
void __fastcall TClientModule1::ClientDataSetServiceAfterPost(TDataSet *DataSet)
{
    ClientDataSetService->ApplyUpdates(-1);
}
//---------------------------------------------------------------------------
void __fastcall TClientModule1::DataModuleCreate(TObject *Sender)
{
	ClientDataSetService->Open();
    ClientDataSetCategory->Open();
}
//---------------------------------------------------------------------------
