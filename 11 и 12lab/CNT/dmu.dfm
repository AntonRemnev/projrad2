object ClientModule1: TClientModule1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 216
  Width = 276
  object SQLConnection: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Filters={}')
    Connected = True
    Left = 72
    Top = 40
  end
  object DSProviderConnection: TDSProviderConnection
    ServerClassName = 'TServerMethods1'
    Connected = True
    SQLConnection = SQLConnection
    Left = 160
    Top = 40
  end
  object ClientDataSetCategory: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderCategory'
    RemoteServer = DSProviderConnection
    AfterPost = ClientDataSetCategoryAfterPost
    AfterScroll = ClientDataSetCategoryAfterScroll
    Left = 64
    Top = 96
    object ClientDataSetCategoryID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ClientDataSetCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 200
    end
  end
  object ClientDataSetService: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderService'
    RemoteServer = DSProviderConnection
    AfterPost = ClientDataSetServiceAfterPost
    Left = 160
    Top = 96
    object ClientDataSetServiceID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object ClientDataSetServiceCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Required = True
    end
    object ClientDataSetServiceNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 240
    end
    object ClientDataSetServicePRICE: TSingleField
      FieldName = 'PRICE'
    end
    object ClientDataSetServiceDURATION: TIntegerField
      FieldName = 'DURATION'
    end
  end
end
