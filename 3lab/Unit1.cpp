//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buConnectClick(TObject *Sender)
{
	IdFTP->Host = "46.188.34.242";
	IdFTP->Username = "test"; //anonimous
	IdFTP->Password = "test";
	IdFTP->Passive = True;
	IdFTP->Connect();
	IdFTP->ChangeDir("/ftp/");
	//
	IdFTP->List(lb->Items,"",false);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	UnicodeString xExt = Ioutils::TPath::GetExtension(Item->Text);
	TMemoryStream *x = new TMemoryStream() ;
	try {
		if (xExt == ".txt") {
			tc->ActiveTab = tiText;
			IdFTP->Get(Item->Text, x, true);
			x->Seek(0,0);
			me->Lines->LoadFromStream(x);
		} else
		if (xExt == ".png" || xExt == ".jpg" ) {
			tc->ActiveTab = tiImage;
			IdFTP->Get(Item->Text, x, true);
			im->Bitmap->LoadFromStream(x);
		} else
		//if (xExt == ".mp4" || xExt == ".avi" || xExt == ".gif") {
			tc->ActiveTab = tiOther;
		  //	IdFTP->Get(Item->Text, x, true);
		  //	mp->Play;
		}


	   finally   {
		delete x;
	}
}
//---------------------------------------------------------------------------

