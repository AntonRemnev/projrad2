//---------------------------------------------------------------------------

#ifndef Unit3H
#define Unit3H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFrame3 : public TFrame
{
__published:	// IDE-managed Components
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem1;
	TEdit *Edit1;
	TListBoxItem *ListBoxItem2;
	TEdit *Edit2;
	TListBoxItem *ListBoxItem3;
	TEdit *Edit3;
	TListBoxItem *ListBoxItem4;
	TEdit *Edit4;
private:	// User declarations
public:		// User declarations
	__fastcall TFrame3(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFrame3 *Frame3;
//---------------------------------------------------------------------------
#endif
