//---------------------------------------------------------------------------

#ifndef Unit4H
#define Unit4H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFrame4 : public TFrame
{
__published:	// IDE-managed Components
	TListBox *ListBox2;
	TListBoxItem *ListBoxItem5;
	TEdit *Edit5;
	TListBoxItem *ListBoxItem6;
	TEdit *Edit6;
	TListBoxItem *ListBoxItem7;
	TEdit *Edit7;
private:	// User declarations
public:		// User declarations
	__fastcall TFrame4(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFrame4 *Frame4;
//---------------------------------------------------------------------------
#endif
