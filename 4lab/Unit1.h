//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include "Unit2.h"
#include "Unit3.h"
#include "Unit4.h"
#include "Unit5.h"
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TToolBar *ToolBar1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TButton *Button1;
	TToolBar *ToolBar2;
	TButton *Button4;
	TButton *Button5;
	TToolBar *ToolBar3;
	TButton *Button8;
	TToolBar *ToolBar4;
	TButton *Button9;
	TButton *Button10;
	TFrame2 *Frame21;
	TFrame3 *Frame31;
	TFrame4 *Frame41;
	TFrame5 *Frame51;
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Frame21Button3Click(TObject *Sender);
	void __fastcall Frame21Button2Click(TObject *Sender);
	void __fastcall Frame51Button7Click(TObject *Sender);
	void __fastcall Frame51Button6Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
