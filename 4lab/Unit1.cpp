//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "Unit2"
#pragma link "Unit3"
#pragma link "Unit4"
#pragma link "Unit5"
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->ActiveTab= TabItem2;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button4Click(TObject *Sender)
{
    tc->ActiveTab= TabItem1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button5Click(TObject *Sender)
{
    tc->ActiveTab= TabItem3;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button9Click(TObject *Sender)
{
	tc->ActiveTab= TabItem2;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button10Click(TObject *Sender)
{
     tc->ActiveTab= TabItem4;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button8Click(TObject *Sender)
{
    tc->ActiveTab= TabItem3;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Frame21Button3Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Frame21Button2Click(TObject *Sender)
{
    tc->ActiveTab= TabItem2;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Frame51Button7Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::Frame51Button6Click(TObject *Sender)
{
    tc->ActiveTab = TabItem2;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab=TabItem1;
}
//---------------------------------------------------------------------------

