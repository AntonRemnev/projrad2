//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit2.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFrame2 *Frame2;
//---------------------------------------------------------------------------
__fastcall TFrame2::TFrame2(TComponent* Owner)
	: TFrame(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFrame2::Button2Click(TObject *Sender)
{
    fm->tc->TabIndex=1;
}
//---------------------------------------------------------------------------
void __fastcall TFrame2::Button3Click(TObject *Sender)
{
	fm->Close();
}
//---------------------------------------------------------------------------
